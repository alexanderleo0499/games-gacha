import Particle from "../classes/Particle";
import { CanvasEl } from "./canvas";

export interface ContainerProps {
  canvasEl: CanvasEl;
}
