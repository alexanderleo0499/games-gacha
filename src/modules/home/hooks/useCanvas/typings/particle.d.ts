import Container from "../classes/Container";
import { CanvasEl, Position } from "./canvas";

export interface Velocity {
  x: number;
  y: number;
}

export interface ParticleProps {
  canvasEl: CanvasEl;
  position: Position;
  radius: number;
  container: Container;
  color: string;
}
