import { RefObject, useEffect, useState } from "react";
import Container from "./classes/Container";
import { useAppSelector } from "@src/lib/hooks/useRedux";

const useCanvas = (ref: RefObject<HTMLCanvasElement>) => {
  const [container, setContainer] = useState<Container | null>(null);

  const { isShuffle } = useAppSelector((state) => state.home);

  const [initial, setInitial] = useState<boolean>(true);
  useEffect(() => {
    window.addEventListener("load", () => {
      if (!ref.current) return;
      const canvas = ref.current;
      const context = canvas.getContext("2d");

      if (!context) return;

      //settings
      canvas.width = window.innerWidth;
      canvas.height = window.innerHeight;

      const c = new Container({
        canvasEl: {
          canvas,
          context,
        },
      });

      setContainer(c);

      function animate() {
        requestAnimationFrame(animate);
        context?.clearRect(0, 0, canvas.width, canvas.height);
        c.render();
      }

      animate();
    });
  }, [ref, container]);

  useEffect(() => {
    if (!container) return;
    setInitial(false);
    if (initial) return;
    container.handleStartStop();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [container, isShuffle]);
};

export default useCanvas;
