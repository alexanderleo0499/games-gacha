import { CanvasEl, Position } from "../typings/canvas";
import { ContainerProps } from "../typings/container";
import { randomIntFromInterval } from "../utils";
import Particle from "./Particle";

class Container {
  canvasEl: CanvasEl = {} as CanvasEl;

  domRect: DOMRect = {} as DOMRect;

  rewardRect: DOMRect = {} as DOMRect;

  particles: Particle[] = [];

  numberOfParticles = 50;

  constructor({ canvasEl }: ContainerProps) {
    this.canvasEl = canvasEl;

    const container = document.getElementById("container");
    if (!container) return;
    const containerRect = container?.getClientRects();
    if (!containerRect) return;
    this.domRect = containerRect[0];

    const rewardZone = document.getElementById("reward-zone");
    if (!rewardZone) return;
    const rewardZoneRect = rewardZone.getClientRects();
    if (!rewardZoneRect) return;

    this.rewardRect = rewardZoneRect[0];

    // Start a new Path
    this.canvasEl.context.beginPath();
    this.canvasEl.context.moveTo(0, 0);
    this.canvasEl.context.lineTo(300, 150);

    this.createParticles();
  }

  draw() {
    const { context, canvas } = this.canvasEl;
    const { x, y, bottom, height, width, left, right, top } = this.domRect;

    context.save();
    context.strokeStyle = "#fca5a5";
    context.lineWidth = 4;
    context.lineCap = "round";
    context.beginPath();
    context.moveTo(left, height);
    context.lineTo(right, height);
    context.lineTo(right, top);
    context.lineTo(left, top);
    context.closePath();
    context.stroke();

    // context.beginPath();
    // context.moveTo(left, top);
    // context.lineTo(right, top);
    // context.stroke();
    context.restore();
  }

  createParticles() {
    for (let i = 0; i < this.numberOfParticles; i++) {
      const radius = Math.floor(Math.random() * 30 + 10);
      this.particles.push(
        new Particle({
          color: `hsl(${i + 5},100%, 50%)`,
          canvasEl: this.canvasEl,
          position: {
            x: randomIntFromInterval(
              this.domRect.left + radius * 2,
              this.domRect.right - radius * 2
            ),
            y: randomIntFromInterval(
              this.domRect.top + radius * 2,
              this.domRect.bottom - radius * 2
            ),
          },
          radius,
          container: this,
        })
      );
    }
  }

  render() {
    this.draw();
    this.particles.forEach((p) => p.update());
  }

  handleStartStop() {
    this.particles.forEach((p) => {
      p.toggleStart();
      p.choosen = false;
    });
    const randomChoosen = randomIntFromInterval(0, this.particles.length - 1);
    this.particles[randomChoosen].choosen = true;
  }
}

export default Container;
