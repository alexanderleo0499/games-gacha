import { CanvasEl, Position } from "../typings/canvas";
import { ParticleProps, Velocity } from "../typings/particle";
import { handleGetRandomVelocity } from "../utils";
import Container from "./Container";

class Particle {
  position: Position = {
    x: 0,
    y: 0,
  };

  canvasEl: CanvasEl = {} as CanvasEl;

  radius = 0;

  velocity: Velocity = {
    x: 0,
    y: Math.random() * 10 + 5,
  };

  container: Container = {} as Container;

  color = "white";

  stop = true;

  choosen = false;

  constructor({ canvasEl, position, radius, container, color }: ParticleProps) {
    this.canvasEl = canvasEl;
    this.position = position;
    this.radius = radius;
    this.container = container;
    this.color = color;
  }

  draw() {
    const { context } = this.canvasEl;
    const { x, y } = this.position;
    context.fillStyle = this.color;
    context.beginPath();
    context.arc(x, y, this.radius, 0, Math.PI * 2);
    context.fill();
    context.stroke();
  }

  update() {
    this.draw();

    // this.velocity.y += 0.09 * this.radius;
    this.position.y += this.velocity.y;
    this.position.x += this.velocity.x;

    const { canvas, context } = this.canvasEl;

    const size = this.radius;

    if (this.stop) {
      this.velocity.x = 0;
    }

    //bottom
    const choose = this.choosen && this.stop;
    if (this.position.y >= this.container.domRect.height - size && !choose) {
      if (!this.stop) this.velocity.y *= -1;
      this.position.y = this.container.domRect.height - size;
    }
    //top
    if (this.position.y <= this.container.domRect.top + size) {
      this.velocity.y *= -1;
      this.position.y = this.container.domRect.top + size;
    }

    //left
    if (this.position.x <= this.container.domRect.left + size) {
      this.velocity.x *= -1;
      this.position.x = this.container.domRect.left + size;
    }

    //right
    if (this.position.x >= this.container.domRect.right - size) {
      this.velocity.x *= -1;
      this.position.x = this.container.domRect.right - size;
    }

    if (this.choosen) {
      this.velocity.x = 0;

      const { left, right } = this.container.rewardRect;
      const rewardLenght = right - left;
      this.position.x = this.container.domRect.left + rewardLenght / 2;
      if (this.position.y >= this.container.rewardRect.top - this.radius) {
        this.velocity.y = 0;
        this.position.y = this.container.rewardRect.top - this.radius;
      }
    }
  }

  toggleStart() {
    this.stop = !this.stop;
    this.velocity = {
      x: Math.random() * 10 - 5,
      y: Math.random() * 10 + 5,
    };
  }
}

export default Particle;
