import React, { useRef } from "react";
import Canvas from "../../components/canvas";
import useCanvas from "./hooks/useCanvas";
import { useAppDispatch, useAppSelector } from "@src/lib/hooks/useRedux";
import { setIsShuffle } from "./store/slices/home.slice";

const HomePage: React.FC = () => {
  const ref = useRef<HTMLCanvasElement>(null);
  const { isShuffle } = useAppSelector((state) => state.home);
  const dispatch = useAppDispatch();

  useCanvas(ref);

  return (
    <div className="flex  justify-center w-full min-h-screen pt-[10%] relative">
      <div
        className="w-[80%] h-[60vh] absolute bg-slate-300 rounded-lg shadow"
        id="container"
      ></div>
      <Canvas
        className=" w-full h-full bg-red-100 absolute left-0 top-0 "
        ref={ref}
      />
      <div className="absolute top-[61vh] w-[80vw] rounded flex items-center justify-end bg-red-200 p-3">
        <button
          className={`rounded-full  w-10 h-10 ${
            isShuffle ? "bg-red-700" : "bg-red-500 shadow-2xl shadow-black"
          }`}
          onClick={() => dispatch(setIsShuffle(!isShuffle))}
          id="btn-play"
        ></button>
      </div>

      <div
        className="absolute top-[92vh] w-[25vw] rounded-bl-md left-[10vw] bg-rose-800 h-[2vh]"
        id="reward-zone"
      ></div>
      <div className="absolute top-[68vh] w-[55vw] rounded-br-md left-[35vw] bg-rose-800 h-[26vh]"></div>
      <div className="absolute top-[68vh] w-[2vw] rounded-br-md left-[10vw] bg-rose-800 h-[26vh] rounded-b-md"></div>
    </div>
  );
};
export default HomePage;
