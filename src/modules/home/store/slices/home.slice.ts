import { PayloadAction } from "@reduxjs/toolkit";
import { createSlice } from "@reduxjs/toolkit";
import { HomeSlice } from "@src/modules/home/typings/slice";

const initialState: HomeSlice = {
  isShuffle: false,
};

const homeSlice = createSlice({
  name: "home-slice",
  initialState,
  reducers: {
    setIsShuffle(state, action: PayloadAction<boolean>) {
      state.isShuffle = action.payload;
    },
  },
});

export const { setIsShuffle } = homeSlice.actions;

export default homeSlice;
